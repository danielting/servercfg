#!/bin/bash
#Install Odoo 8.0 (OpenERP) in Dev Mode

wget -O- https://raw.githubusercontent.com/odoo/odoo/8.0/odoo.py | python
sudo su - postgres -c "createuser -s $USER"
cd odoo
sudo apt-get install -y npm nginx python-pip
sudo ln -s /usr/bin/nodejs /usr/bin/node
pip install -r requirements.txt
sudo npm install -g less less-plugin-clean-css
screen -d -m "./odoo.py"
echo "Done!"